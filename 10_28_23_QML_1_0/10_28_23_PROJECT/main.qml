import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5


Window {
    width: 600
    height: 600
    visible: true
    title: qsTr("Kriper")
    color: "green"

    Item {
        width: 600
        height: 600

        My_Comp {
            comColor: "black"
            anchors.left: parent.left
            anchors.top: parent.top
        }
        My_Comp {
            comColor: "black"
            anchors.left: parent.left
            anchors.top: parent.top
            comX: 100
        }
        My_Comp {
            comColor: "black"
            anchors.left: parent.left
            anchors.top: parent.top
            comY: 100
        }
        My_Comp {
            comColor: "black"
            anchors.left: parent.left
            anchors.top: parent.top
            comX: 100
            comY: 100
        }


        My_Comp {
            comColor: "black"
            anchors.top: parent.top
            anchors.right: parent.right
            comX: -100
        }
        My_Comp {
            comColor: "black"
            anchors.top: parent.top
            anchors.right: parent.right
            comX: -200
        }
        My_Comp {
            comColor: "black"
            anchors.top: parent.top
            anchors.right: parent.right
            comX: -100
            comY: 100
        }
        My_Comp {
            comColor: "black"
            anchors.top: parent.top
            anchors.right: parent.right
            comX: -200
            comY: 100
        }



        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comY: -100
        }
        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comX: -100
            comY: -100
        }


        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
        }
        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comX: -100
        }
        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comX: -200
        }
        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comX: 100
        }


        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comY: 100
        }
        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comX: -100
            comY: 100
        }
        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comX: -200
            comY: 100
        }
        My_Comp {
            comColor: "black"
            anchors.centerIn: parent
            comX: 100
            comY: 100
        }


        My_Comp {
            comColor: "black"
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            comX: 100
            comY: -100
        }
        My_Comp {
            comColor: "black"
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            comX: -200
            comY: -100
        }

    }
}
